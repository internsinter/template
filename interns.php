<?php 
	//Template Name: interns
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/interns.css">
	<title>Document</title>
</head>
<body>
	<section id="full-Content">
		<!-- This section it for import the header and Nav-->
		<section id="header">
			<?php get_header(); ?>
			
		</section>
		<!-- This section it for import the content frontpage or entries if you use a sidebar  upload into this -->
		<section id="content">
		
				

			<?php 
			query_posts('category_name=interns');
			if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			?>
			<section class="profile">
			<?php the_content(); ?>
			<p class="name"><?php the_title(); ?></p>
			</section>
			
			

			


			<?php
			endwhile; else:
			?>
			<p>
				<?php 
				_e('Sorry, no posts matched your criteria.'); 
				?>
			</p>
			<?php endif; ?>
		</section>
		
		<!-- This section it for import the footer-->
		<section id="footer">
			<?php get_footer(); ?>
		</section>
	</section>
	
	<script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/functions.js"></script>    
</body>
</html>
